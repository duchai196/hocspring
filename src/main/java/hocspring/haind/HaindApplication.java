package hocspring.haind;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HaindApplication {

    public static void main(String[] args) {
        SpringApplication.run(HaindApplication.class, args);
    }

}
