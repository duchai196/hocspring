package hocspring.haind.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class User {
    @Id
    String id;
    String token;
    String name;
    String email;
    String uid;
    String avatar;
    String avatarOriginal;

//    public User(String token, String name, String email, String uid, String avatar, String avatarOriginal) {
//
//    }

    public User(String token, String name, String email, String uid, String avatar, String avatarOriginal) {
        this.token = token;
        this.name = name;
        this.email = email;
        this.uid = uid;
        this.avatar = avatar;
        this.avatarOriginal = avatarOriginal;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatarOriginal() {
        return avatarOriginal;
    }

    public void setAvatarOriginal(String avatarOriginal) {
        this.avatarOriginal = avatarOriginal;
    }
}
