package hocspring.haind.repository;

import hocspring.haind.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
    public User findByUid(String UID);

    public List<User> findByName(String name);
}
