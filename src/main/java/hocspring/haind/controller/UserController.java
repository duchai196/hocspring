package hocspring.haind.controller;

import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {
    @PostMapping("/api/user")
    @ResponseBody
    public String test(@RequestParam(name = "id", required = false) String fooId, @RequestParam String name) {
        return "xin chao " + fooId + " - " + name;
    }
}

